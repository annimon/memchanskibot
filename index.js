const TelegramBot = require('node-telegram-bot-api');
const sqlite = require('sqlite-sync');
const _ = require('lodash');
const config = require('./config.json');

sqlite.connect('app.db');
sqlite.run('CREATE TABLE IF NOT EXISTS bookmarks (\n'
  + '`id`       INTEGER PRIMARY KEY AUTOINCREMENT,\n'
  + '`key`      TEXT NOT NULL UNIQUE,\n'
  + '`type`     TEXT NOT NULL,\n' // text, audio, document, photo, sticker, video, voice, video_note
  + '`public`   INTEGER NOT NULL,\n'
  + '`from_id`  INTEGER NOT NULL,\n'
  + '`date`     INTEGER NOT NULL,\n'
  + '`file_id`  TEXT,\n'
  + '`text`     TEXT,\n'
  + '`thumb`    TEXT,\n'
  + '`debug`    TEXT\n'
  + ');', function(res) {
  if (res.error)
    throw res.error;
});

const bot = new TelegramBot(config.token, {
  polling: true,
  filepath: false
});

bot.onText(/\/start/, (msg) => {
  bot.sendMessage(msg.chat.id, "Привет, я бот, упрощающий доступ "
    + "к часто используемым (локальным) мемасикам.\n"
    + "Просто отправь мне название мемаса в формате\n"
    + "/mem название\n"
    + "и я пришлю тебе картинку, стикер, гифку или голосовое сообщение из списка.\n"
    + "Список мемасиков можно просмотреть, вызвав команду /list",
    {parse_mode : "markdown"}
  );
});

// Inline
bot.on('inline_query', (msg) => {
  const entriesPerPage = 25;
  const offset = parseInt(msg.offset) || 0;
  var messages, count;
  var typeMatch, searchMatch
  if ((typeMatch = /\:type (text|audio|document|photo|sticker|voice|video_note|video)/.exec(msg.query)) != null) {
    // Filter by media type
    count = sqlite.run(
      'SELECT COUNT(*) as cnt FROM bookmarks WHERE `type` = ?',
      [typeMatch[1]])[0].cnt;
    messages = sqlite.run(
      'SELECT * FROM bookmarks WHERE `type` = ? ORDER BY id DESC LIMIT ' + entriesPerPage + ' OFFSET ' + offset,
      [typeMatch[1]]);
  } else if ((searchMatch = /\:s ([^;\"'`]+)/.exec(msg.query)) != null) {
    // Filter by 'debug' search
    const searchParam = '%' + searchMatch[1].replace('%', '\\%') + '%';
    count = sqlite.run(
      'SELECT COUNT(*) as cnt FROM bookmarks WHERE `debug` LIKE ?',
      [searchParam])[0].cnt;
    messages = sqlite.run(
      'SELECT * FROM bookmarks WHERE `debug` LIKE ? LIMIT ' + entriesPerPage + ' OFFSET ' + offset,
      [searchParam]);
  } else if (!/[^;\"'`]+/.test(msg.query)) {
    // All data
    count = sqlite.run('SELECT COUNT(*) as cnt FROM bookmarks')[0].cnt;
    messages = sqlite.run(
      'SELECT * FROM bookmarks ORDER BY id DESC LIMIT ' + entriesPerPage + ' OFFSET ' + offset);
  } else {
    // Query
    const keyParam = '%' + msg.query.replace('%', '\\%') + '%';
    count = sqlite.run(
      'SELECT COUNT(*) as cnt FROM bookmarks WHERE `key` LIKE ?',
      [keyParam])[0].cnt;
    messages = sqlite.run(
      'SELECT * FROM bookmarks WHERE `key` LIKE ? LIMIT ' + entriesPerPage + ' OFFSET ' + offset,
      [keyParam]);
  }

  let results = _.map(messages, (mem, i) => {
    switch (mem.type) {
      case 'audio':
        const audio = JSON.parse(mem.debug).audio;
        if (!('title' in audio)) return false;
        return {
          id: msg.id + i * 21,
          type: 'audio',
          audio_file_id: mem.file_id
        };
      case 'document':
        const document = JSON.parse(mem.debug).document;
        const mime = document.mime_type;
        if (mime == "video/mp4") {
          return {
            id: msg.id + i * 31,
            type: 'mpeg4_gif',
            title: mem.key,
            mpeg4_file_id: mem.file_id
          };
        }
        return {
          id: msg.id + i * 41,
          type: 'document',
          title: mem.key,
          document_file_id: mem.file_id
        };
      case 'photo': return {
          id: msg.id + i * 47,
          type: 'photo',
          title: mem.key,
          photo_file_id: mem.file_id
        };
      case 'sticker': return {
          id: msg.id + i * 57,
          type: 'sticker',
          sticker_file_id: mem.file_id
        };
      case 'text': return {
          id: msg.id + i * 67,
          type: 'article',
          title: mem.key,
          description: mem.text.substring(0, 60),
          input_message_content: {
            message_text: mem.text,
            parse_mode: 'markdown'
          }
        };
      case 'video': return {
          id: msg.id + i * 71,
          type: 'video',
          title: mem.key,
          video_file_id: mem.file_id
        };
      case 'video_note':
        return false;
      case 'voice': return {
          id: msg.id + i * 93,
          type: 'voice',
          title: mem.key,
          voice_file_id: mem.file_id
        };
      }
      return false;
  });
  results = _.filter(results, (m) => m !== false);

  var next_offset = "";
  if (offset < count) {
    next_offset = offset + entriesPerPage;
  }
  bot.answerInlineQuery(msg.id, results, {next_offset});
});

bot.onText(/^\/mem ([^;\"'`]+)$/, (msg, match) => {
  const key = match[1].trim().toLowerCase();
  const mem = getMeme(key);
  if (!mem.exists) return;
  sendMeme(msg.chat.id, mem);
});

function sendMeme(chatId, mem) {
  switch (mem.type) {
    case 'audio':
      bot.sendAudio(chatId, mem.file_id);
      break;
    case 'document':
      bot.sendDocument(chatId, mem.file_id);
      break;
    case 'photo':
      bot.sendPhoto(chatId, mem.file_id);
      break;
    case 'sticker':
      bot.sendSticker(chatId, mem.file_id);
      break;
    case 'text':
      // legacy mode
      const debug = JSON.parse(mem.debug);
      bot.forwardMessage(chatId, debug.from.id, debug.message_id);
      break;
    case 'video':
      bot.sendVideo(chatId, mem.file_id);
      break;
    case 'video_note':
      bot.sendVideoNote(chatId, mem.file_id);
      break;
    case 'voice':
      bot.sendVoice(chatId, mem.file_id);
      break;
  }
}

// List callback

bot.onText(/\/list/, function (msg) {
  const memes = getMemes();
  const memesPerPage = 50;
  const maxPages = Math.floor(memes.length / memesPerPage) + 1;
  for (let page = 0; page < maxPages; page++) {
    const buttons = _.chain(memes)
        .map((r) => ({text: r.key, callback_data: r.key}))
        .drop(page * memesPerPage)
        .take(memesPerPage)
        .value();
    const info = buttons[0].text.charAt(0) + ".." + buttons[buttons.length - 1].text.charAt(0);
    
    bot.sendMessage(msg.chat.id, 'Список мемасиков, ожидающих своего часа (' + info + '):', {
      reply_markup: {
        inline_keyboard: _.chunk(buttons, 3)
      }
    });
  }
});


// Handle callback queries
bot.on('callback_query', function (callbackQuery) {
  const msg = callbackQuery.message;
  const mem = getMeme(callbackQuery.data);
  if (!mem.exists) return;
  sendMeme(msg.chat.id, mem);
});


// Add meme commands group
var addMode = {};

bot.onText(/^\/add ([^;\"'`]+)$/, (msg, match) => {
  if (msg.from.id != msg.chat.id) return;

  const key = match[1].trim().toLowerCase();
  const meme = getMeme(key);
  if (meme.exists && (meme.from_id != msg.from.id)) {
    bot.sendMessage(msg.chat.id, 'Извините, мемасик "' + key + '" уже существует.');
    return;
  }

  addMode[msg.from.id] = key;
  var prefix = 'Отлично, теперь';
  if (meme.exists) {
    prefix = 'Мемас существует. Чтобы его перезаписать,';
  }
  bot.sendMessage(msg.chat.id,
    prefix + ' отправьте сообщение с мемасиком, '
      + 'который хотите сохранить. Я добавлю его под именем "' + key + '".\n'
      + 'Чтобы отменить добавление, отправьте /cancel');
});

bot.on('message', function (msg) {
  if (msg.from.id != msg.chat.id) return;
  if (!(msg.from.id in addMode)) return;

  const key = addMode[msg.from.id];
  delete addMode[msg.chat.id];
  if (typeof msg.text !== 'undefined' && msg.text.toLowerCase().indexOf("/cancel") === 0) {
    bot.sendMessage(msg.chat.id, 'Команда отменена.');
    return;
  }

  const meme = getMeme(key);
  if (meme.exists && (meme.from_id != msg.from.id)) {
    bot.sendMessage(msg.chat.id, 'Пока вы думали, мемасик "' + key + '" уже кто-то занял :(');
    return;
  }

  const data = {
    key: key,
    from_id: msg.from.id,
    date: msg.date,
    public: true,
    debug: JSON.stringify(msg)
  };
  if ('text' in msg) {
    data.type = 'text';
    data.text = msg.text;
  } else if ('photo' in msg) {
    const photo = msg.photo;
    data.type = 'photo';
    data.file_id = photo[photo.length - 1].file_id;
    data.thumb = photo[0].file_id;
  } else {
    const types = ['audio', 'document', 'sticker', 'video', 'voice', 'video_note'];
    for (let i in types) {
      const type = types[i];
      if (type in msg) {
        data.type = type;
        data.file_id = msg[type].file_id;
        if ('thumb' in msg[type]) {
          data.thumb = msg[type].thumb.file_id;
        }
        break;
      }
    }
  }

  if (meme.exists) {
    sqlite.update("bookmarks", data, {'key' : key}, function (res) {
      var message = '';
      if (res.error) {
        message = "Не удалось обновить мемасик, попробуйте позже :(";
      } else {
        message = "Мемасик обновлён :)";
      }
      bot.sendMessage(msg.chat.id, message);
    });
  } else {
    sqlite.insert("bookmarks", data, function (res) {
      var message = '';
      if (res.error) {
        message = "Не удалось добавить мемасик, попробуйте позже :(";
      } else {
        message = "Мемасик добавлен :)";
      }
      bot.sendMessage(msg.chat.id, message);
    });
  }
});

// Admin commands group

bot.onText(/^\/remove ([^;\"'`]+)$/, (msg, match) => {
  const key = match[1].trim().toLowerCase();
  var canRemove = (msg.from.id == config.admin_id);
  if (!canRemove) {
    const meme = getMeme(key);
    canRemove = meme.exists && (meme.from_id == msg.from.id);
  }
  if (!canRemove) return;
  
  sqlite.run("DELETE FROM `bookmarks` WHERE `key` = ?", [key], (res) => {
    var message = '';
    if (res.error) {
      message = "Не удалось удалить мемасик.\n```\n" + res.error + "\n```";
    } else {
      message = "Мемас удалён.";
    }
    bot.sendMessage(msg.chat.id, message, {parse_mode : "markdown"});
  });
});

// Helpers

function isMemeExists(key) {
  return sqlite.run(
    'SELECT COUNT(*) as cnt FROM bookmarks WHERE `key` = ? LIMIT 1;',
    [key]
  )[0].cnt != 0;
}

function getMeme(key) {
  const data = sqlite.run(
    'SELECT * FROM bookmarks WHERE `key` = ? LIMIT 1;',
    [key]
  );
  if (data.length == 0) {
    return {exists: false};
  }
  data[0].exists = true;
  return data[0];
}

function getMemes() {
  return sqlite.run('SELECT `key` FROM bookmarks;');
}
